package com.hascode.hacking;

import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.animation.TranslateTransitionBuilder;
import javafx.application.Application;
import javafx.geometry.VPos;
import javafx.scene.GroupBuilder;
import javafx.scene.Scene;
import javafx.scene.SceneBuilder;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBuilder;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {
	public static void main(final String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(final Stage stage) throws Exception {
		Text text = TextBuilder.create().text("Fooooooo")
				.font(Font.font("Arial", 70)).textOrigin(VPos.TOP).build();
		TranslateTransition transition = TranslateTransitionBuilder.create()
				.duration(Duration.seconds(10)).node(text).toX(800)
				.interpolator(Interpolator.LINEAR).build();
		Scene scene = SceneBuilder.create().width(500).height(250)
				.root(GroupBuilder.create().children(text).build()).build();
		stage.setScene(scene);
		stage.setTitle("hasCode.com - Java FX Samples");
		stage.show();
		transition.play();
	}

}
